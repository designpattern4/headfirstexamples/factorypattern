# FactoryPattern

This pattern is organized under creational pattern as it deals with the creation of the object.

This design pattern is based on one of the OOPs concept ie. **Encapsulation**. Generally, we write object creation code on client side program but in factory pattern, we encapsulate object creation code inside factory method. So depending on the data provided to the factory, it can return an object of one of several possible classes listed in a program. You can consider using this pattern when:

* Your subclasses need to instantiate objects of some classes based on criteria.
* An application needs to have loose coupling between different classes.

